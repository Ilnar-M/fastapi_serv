Face detection Fast API service with OpenVINO Ultra Light Fast Generic Face Detector. Original project with openvino face detection model: https://github.com/Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB

**Local Running**

1. Clone this repository
2. `cd ./face-cheacker`
3. ```bash
    docker build -f Dockerfile -t facedet_fastapi
   ```
5. ```bash
    docker-compose up
    ```
6. Visit http://localhost:5555/content-analyzer/swagger to see POST request example and try to send image. Use
   the `face-detect` endpoint to submit image.
