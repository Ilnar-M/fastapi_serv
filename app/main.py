from fastapi import FastAPI, UploadFile, File, Response, status, APIRouter
from model_inference import FaceDetect, Status
import uvicorn
from response_examples import post_responses, AdditionalInfo, OkExample, ResponseDict, BadResponseDict, RESPONSE_CODES


MAX_REQ = 12


router = APIRouter()
app = FastAPI(title='OpenVINO FastAPI face checker', docs_url='/content-analyzer/swagger',
              openapi_url='/content-analyzer/openapi.json')


app.include_router(router)
try:
    face_detection_model = FaceDetect(num_requests=MAX_REQ)
except Exception as ex:
    face_detection_model = None
    print(f"Failed to load face detection model: {ex.args[0]}")
    exit(-1)


@app.get('/content-analyzer')
def home() -> str:
    return "OpenVINO FastAPI is ready"


@app.post('/content-analyzer/face-detect', status_code=200, responses=post_responses,
          description='Process POST request and run model inference on received image')
async def find_face(response: Response, file: UploadFile = File(...)):
    """
    Process POST request and run model inference on received image
    :param file: Image
    :param response response code
    :return: String with image name and model response
    """
    print(f'Got file: {file.filename}')
    content = await file.read()
    image_np, image_width, image_height, image_mode, (status_head, status_value, probs, areas, boxes) = \
        await face_detection_model.predict_async(content)
    additional_info = AdditionalInfo(image_width=image_width, image_height=image_height,
                                     image_mode=image_mode, infer_scores=probs,
                                     bbox_areas=areas, bbox_coords=boxes)

    if status_head == Status.SUCCESS:
        return ResponseDict(additional_info=additional_info, result=RESPONSE_CODES['OK'])
    else:
        if status_head == Status.IMAGE_ERROR:
            response.status_code = status.HTTP_400_BAD_REQUEST
        return BadResponseDict(additional_info=additional_info, result=RESPONSE_CODES['BAD'], reason=status_value)


def main():
    uvicorn.run(app, host='0.0.0.0', port=5555, debug=False)


if __name__ == '__main__':
    main()
