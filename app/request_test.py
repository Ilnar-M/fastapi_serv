import sys
import requests
import threading
import signal
import time
import random
import os

ROOT_IMAGE_DIR = r'.\imgs'
THREAD_NUM = 10
IMAGE_EXT = ['.jpg', '.png']


def handler(signum, frame):
    sys.exit(0)


def send_request(filepath: str):
    url = 'http://localhost:5555/model'
    with open(filepath, 'rb') as f:
        r = requests.post(url, files={'file': f, 'type': f'image/{os.path.splitext(filepath)[1:]}'})
        assert r.status_code == 200
        print(f'Image {r.content}, processed')
    f.close()


def main():
    print('Double press CTRL+C to stop')
    threads = []
    if not os.path.exists(ROOT_IMAGE_DIR):
        print(f'There is no files in {ROOT_IMAGE_DIR}')
        exit(-1)

    files = os.listdir(ROOT_IMAGE_DIR)
    assert len(files) >= THREAD_NUM
    for file in files:
        splitext = os.path.splitext(file)
        if len(splitext) != 2:
            continue
        if splitext[1] not in IMAGE_EXT:
            files.remove(file)

    delay = 1
    iterations = len(files) // THREAD_NUM
    remainder = len(files) % THREAD_NUM

    while True:
        for f_index in range(iterations):
            for t_index in range(THREAD_NUM):
                path = os.path.join(ROOT_IMAGE_DIR, files[f_index * THREAD_NUM + t_index])
                threads.append(threading.Thread(target=send_request,
                                                args=(path,)))
                threads[-1].start()

        threads.clear()
        offset = THREAD_NUM * iterations
        for f_index in range(remainder):
            path = os.path.join(ROOT_IMAGE_DIR, files[offset + f_index])
            threads.append(threading.Thread(target=send_request,
                                            args=(path,)))
            threads[-1].start()
        time.sleep(delay)
        delay = random.uniform(0.1, 3)
        threads.clear()


signal.signal(signal.SIGINT, handler)
if __name__ == '__main__':
    main()