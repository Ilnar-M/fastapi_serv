"""
This code uses the onnx model to detect faces from live video or cameras.
"""
import time

import PIL.Image
import cv2
import numpy
import numpy as np
from PIL import Image
import io
import box_utils_numpy as box_utils
from openvino.inference_engine import IECore, Blob, TensorDesc, StatusCode, ExecutableNetwork, InferRequest
import os
import asyncio
from concurrent.futures import ThreadPoolExecutor
from typing import Union, Tuple, Any, Optional, NamedTuple, List

xml_path = 'weights/ultra-lightweight-face-detection-slim-320/FP32/ultra-lightweight-face-detection-slim-320.xml'
bin_path = 'weights/ultra-lightweight-face-detection-slim-320/FP32/ultra-lightweight-face-detection-slim-320.bin'
labels = 'labels.txt'
event_loop = asyncio.get_event_loop()
executor = ThreadPoolExecutor()
FACE_THRESHOLD = 0.8
BBOX_AREA_THRESHOLD = 2500
SUPPORT_MODES = ['RGB', 'BGR']

FACE_DETECTOR = "__FACE_DETECTOR__"
HEAD_POSE = "__HEAD_POSE__"

ie = IECore()
MODELS_ROOT_PATH = 'weights'


def get_models_absolute_path(models_path):
    return os.path.join(MODELS_ROOT_PATH, models_path)


def format_infer_return(pil_pic: Optional[PIL.Image.Image], numpy_image: Optional[np.ndarray], status_code: int,
                        status_value: str, probs: List[float], areas: List[float], bboxes: List[float]) -> \
        Tuple[Optional[np.ndarray], Optional[int], Optional[int], str, Tuple[int, str, list, list, list]]:
    infer_status = (status_code, status_value, probs, areas, bboxes)
    if pil_pic is None:
        return None, None, None, 'Undefined', infer_status
    else:
        return numpy_image, pil_pic.width, pil_pic.height, pil_pic.mode, infer_status


class HeadPoseTuple(NamedTuple):
    Pitch: float
    Yaw: float
    Roll: float


class Status:
    def __init__(self):
        pass

    SUCCESS = 0
    CHANNELS_ERROR = 1
    NO_FACE = 2
    MULTIPLE_FACES = 3
    IMAGE_ERROR = 4
    SMALL_FACE = 5
    INFERENCE_ERROR = 11
    CAMERA_TOO_LOW = 12
    CAMERA_TOO_HIGH = 13
    codes = {
        0: '__COMPLETED__',
        1: '__IMAGE_MUST_BE_RGB__',
        2: '__FACE_NOT_DETECTED__',
        3: '__MULTIPLE_FACES__',
        4: '__CANT_OPEN_IMAGE__',
        5: '__FACE_TOO_SMALL__',
        11: '__INFERENCE_ERROR__',
        12: '__CAMERA_TOO_LOW__',
        13: '__CAMERA_TOO_HIGH__'
    }


class HeadDetectResultTuple(NamedTuple):
    status_code: int
    status_str: str
    head_pose_tuple: HeadPoseTuple


class FaceDetect:
    def __init__(self, labels_path=labels, num_requests=1):
        self.__ie_core_handler = IECore()
        self.__network: ExecutableNetwork = self.__ie_core_handler.read_network(model=xml_path, weights=bin_path)
        self.__input_h, self.__input_w = self.__network.input_info['input'].input_data.shape[2:]
        assert num_requests > 0
        self.__exec_net: ExecutableNetwork = self.__ie_core_handler.load_network(self.__network, device_name='CPU',
                                                                                 num_requests=num_requests)
        self.__requests_list = self.__exec_net.requests
        self.__tensor_desc = TensorDesc(precision='FP32', dims=(1, 3, self.__input_h, self.__input_w), layout='NCHW')
        self.__class_names = [name.strip() for name in open(labels_path).readlines()]
        self.max_requests = num_requests

    @staticmethod
    def __decode_model_output(width, height, confidences, boxes, prob_threshold: float = FACE_THRESHOLD,
                              iou_threshold=0.3,
                              top_k=-1) -> \
            Union[np.array, np.array, np.array]:

        """
        Get prediction from module output. Drawing bbox on original image
        :param width: original image width
        :param height: original image height
        :param confidences: model output scores
        :param boxes: model output bboxes
        :param prob_threshold: upper threshold for prediction
        :param iou_threshold: iou_theshold for bbox
        :param top_k: keep results flag for NMS
        :return: tuple with predicted bboxes, predicted labels and scores for each label
        """

        boxes = boxes[0]
        confidences = confidences[0]
        picked_box_probs = []
        picked_labels = []
        for class_index in range(1, confidences.shape[1]):
            probs = confidences[:, class_index]
            mask = probs >= prob_threshold
            probs = probs[mask]
            if probs.shape[0] == 0:
                continue
            subset_boxes = boxes[mask, :]
            box_probs = np.concatenate([subset_boxes, probs.reshape(-1, 1)], axis=1)
            box_probs = box_utils.hard_nms(box_probs,
                                           iou_threshold=iou_threshold,
                                           top_k=top_k,
                                           )
            picked_box_probs.append(box_probs)
            picked_labels.extend([class_index] * box_probs.shape[0])
        if not picked_box_probs:
            return np.array([]), np.array([]), np.array([])
        picked_box_probs = np.concatenate(picked_box_probs)
        picked_box_probs[:, 0] *= width
        picked_box_probs[:, 1] *= height
        picked_box_probs[:, 2] *= width
        picked_box_probs[:, 3] *= height
        return picked_box_probs[:, :4].astype(np.int32), np.array(picked_labels), picked_box_probs[:, 4]

    def __get_prepared_data(self, content: bytes) -> Tuple[PIL.Image.Image, np.array, np.array, int, int]:
        try:
            pic = Image.open(io.BytesIO(content))
        except PIL.UnidentifiedImageError:
            raise PIL.UnidentifiedImageError(Status.IMAGE_ERROR)
        if pic.mode not in SUPPORT_MODES:
            raise ValueError(Status.CHANNELS_ERROR)
        image_np_src = np.asarray(pic, dtype=np.uint8)
        image = cv2.resize(image_np_src, (self.__input_w, self.__input_h))
        image = np.transpose(image, [2, 0, 1])
        image = np.expand_dims(image, axis=0)
        return pic, image.astype(np.float32), image_np_src, pic.width, pic.height

    def __get_prediction(self, src_width: int, src_height: int, output) -> Tuple[int, str, list, list, list]:
        """
        Returns prediction result from model output
        :param src_width: original image width
        :param src_height: original image height
        :param output: model output
        :return: Tuple with prediction result
        """
        output_blobs = output.output_blobs
        boxes = output_blobs['boxes'].buffer
        scores = output_blobs['scores'].buffer
        boxes, out_labels, probs = self.__decode_model_output(src_width, src_height, scores, boxes)

        if boxes.shape[0] == 0:
            return Status.NO_FACE, Status.codes[Status.NO_FACE], probs.tolist(), [], []
        elif boxes.shape[0] > 1:
            areas = []
            bboxes = []
            for i in range(boxes.shape[0]):
                box = boxes[i, :]
                bbox_width = box[2] - box[0]
                bbox_height = box[3] - box[1]
                areas.append(int(bbox_height * bbox_width))
                bboxes.append(box.tolist())
            return Status.MULTIPLE_FACES, Status.codes[Status.MULTIPLE_FACES], probs.tolist(), areas, bboxes

        label = f"{self.__class_names[out_labels[0]]}"
        bbox_width = boxes[0][2] - boxes[0][0]
        bbox_height = boxes[0][3] - boxes[0][1]
        area = bbox_height * bbox_width
        if label == 'face' and area >= BBOX_AREA_THRESHOLD:
            return Status.SUCCESS, Status.codes[Status.SUCCESS], probs.tolist(), [area.tolist()], boxes.tolist()
        elif label == 'face' and area < BBOX_AREA_THRESHOLD:
            return Status.SMALL_FACE, Status.codes[Status.SMALL_FACE], probs.tolist(), [area.tolist()], boxes.tolist()

    def predict_sync(self, content: bytes) -> Tuple[Optional[np.ndarray], Optional[int],
                                                    Optional[int], str, Tuple[int, str, list, list, list]]:
        """
        Perform synchronous inference on the image
        :param content: image (e.g. JPG) in byte representation
        :return: string value of prediction
        """
        time_time = time.time()  # time measurement
        pil_pic = None
        try:
            pil_pic, image, image_src, src_width, src_height = self.__get_prepared_data(content)
        except ValueError as v_err:
            return format_infer_return(pil_pic, None, v_err.args[0], Status.codes[int(v_err.args[0])], [], [], [])
        except PIL.UnidentifiedImageError as pil_error:
            return format_infer_return(None, None, pil_error.args[0], Status.codes[int(pil_error.args[0])], [], [], [])
        inference_request = self.__requests_list[0]  # getting first inference request

        input_blob = Blob(inference_request.input_blobs['input'].tensor_desc, image)  # creating universal container
                                                                                      # for inference
        input_blob_name = next(iter(inference_request.input_blobs))  # getting input blob name
        inference_request.set_blob(blob_name=input_blob_name,  #
                                   blob=input_blob)
        inference_request.infer()  # start model inference
        result_time = time.time() - time_time

        print("Inference completed in:{}".format(result_time))

        result = format_infer_return(pil_pic, image_src, *self.__get_prediction(src_width, src_height, inference_request))
        return result

    async def predict_async(self, content: bytes) -> Tuple[Optional[np.ndarray], Optional[int], Optional[int], str,
                                                           Tuple[int, str, list, list, list]]:
        """
        Performs inference in asynchronous mode.
        See https://docs.openvino.ai/latest/openvino_docs_OV_UG_Infer_request.html for details
        """
        pil_pic = None
        try:
            pil_pic, image, image_src, src_width, src_height = self.__get_prepared_data(content)
        except ValueError as v_err:
            return format_infer_return(pil_pic, None, v_err.args[0], Status.codes[int(v_err.args[0])], [], [], [])
        except PIL.UnidentifiedImageError as pil_error:
            return format_infer_return(None, None, pil_error.args[0], Status.codes[int(pil_error.args[0])], [], [], [])
        req_id = self.__exec_net.get_idle_request_id()
        result_handler = self.__exec_net.start_async(request_id=req_id,
                                                     inputs={'input': image})  # Start async inference
        while True:
            result = result_handler.wait(timeout=0)  # Blocking until inference complete
            if result == StatusCode.OK:
                break
            elif result == StatusCode.UNEXPECTED:
                return format_infer_return(pil_pic, image_src, Status.INFERENCE_ERROR, Status.codes[Status.INFERENCE_ERROR], [], [], [])
            else:
                await asyncio.sleep(0)
        result = format_infer_return(pil_pic, image_src, *self.__get_prediction(src_width, src_height, result_handler))

        return result

    async def predict_in_executor(self, content: bytes) -> Tuple[Optional[np.ndarray], Optional[int], Optional[int],
                                                                 str, Tuple[int, str, list, list, list]]:
        """
        Performs prediction in asynchronous mode with ThreadPoolExecutor (openvino model has only one request)
        """
        return await event_loop.run_in_executor(executor, self.predict_sync, content)


def main() -> None:
    images_path = './Ultra-Light-Fast-Generic-Face-Detector-1MB/imgs'
    paths = os.listdir(images_path)

    model = FaceDetect()  # OpenVINO model
    sum_time = 0

    for path in paths:
        with open(os.path.join(images_path, path), 'rb') as f:
            sum_time += model.predict_sync(f.read())

    print(f'Avg time for openVino {sum_time / len(paths)}')


if __name__ == '__main__':
    main()
