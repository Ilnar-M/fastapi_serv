from typing import List, Optional
from model_inference import Status
from pydantic import BaseModel


RESPONSE_CODES = {
    'OK': 0,
    'BAD': 1
}


class AdditionalInfo(BaseModel):
    image_width: int = None
    image_height: int = None
    image_mode: str
    infer_scores: List
    bbox_areas: List
    bbox_coords: List
    yaw_angle: Optional[float] = None
    pitch_angle: Optional[float] = None
    roll_angle: Optional[float] = None


class ResponseDict(BaseModel):
    additional_info: AdditionalInfo
    result: str


class BadResponseDict(ResponseDict):
    reason: str


class OkExample(BaseModel):
    summary: str
    description: str
    value: ResponseDict


class BadExample(BaseModel):
    summary: str
    description: str
    value: BadResponseDict


additional_info_example = AdditionalInfo(image_width=960, image_height=540,
                                         image_mode='RGB', infer_scores=[0.995884713715],
                                         bbox_areas=[18252], bbox_coords=[48, 224, 165, 380],
                                         yaw_angle=15.1, pitch_angle=2.7, roll_angle=0.0)
additional_info_multiple_faces = AdditionalInfo(image_width=960, image_height=540,
                                                image_mode='RGB', infer_scores=[0.995884713715, 0.899441161],
                                                bbox_areas=[18252, 10560],
                                                bbox_coords=[[48, 224, 165, 380], [60, 312, 180, 400]],
                                                yaw_angle=0, pitch_angle=0, roll_angle=0)
additional_info_small_face = AdditionalInfo(image_width=960, image_height=540,
                                            image_mode='RGB', infer_scores=[0.995884713715],
                                            bbox_areas=[784], bbox_coords=[400, 300, 428, 328],
                                            yaw_angle=0, pitch_angle=0, roll_angle=0)
additional_info_example_no_detect = AdditionalInfo(image_width=960, image_height=540,
                                                   image_mode='RGB', infer_scores=[],
                                                   bbox_areas=[], bbox_coords=[],
                                                   yaw_angle=0, pitch_angle=0, roll_angle=0)
additional_info_example_bad_image = AdditionalInfo(image_width=None, image_height=None,
                                                   image_mode='Undefined', infer_scores=[],
                                                   bbox_areas=[], bbox_coords=[],
                                                   yaw_angle=0, pitch_angle=0, roll_angle=0)
additional_info_example_channels_error = AdditionalInfo(image_width=960, image_height=540,
                                                        image_mode='L', infer_scores=[],
                                                        bbox_areas=[], bbox_coords=[],
                                                        yaw_angle=0, pitch_angle=0, roll_angle=0)
additional_info_example_wrong_angle = AdditionalInfo(image_width=960, image_height=540,
                                                     image_mode='RGB', infer_scores=[0.995884713715],
                                                     bbox_areas=[18252], bbox_coords=[48, 224, 165, 380],
                                                     yaw_angle=-32.0, pitch_angle=-40, roll_angle=20)

responses_200 = {
    '200_ok': OkExample(summary='Image processed and face found',
                        description='OpenVINO processed image and found face',
                        value=ResponseDict(additional_info=additional_info_example,
                                           result=RESPONSE_CODES['OK'])),
    '200_bad': BadExample(summary='Image processed and face not found',
                          description='OpenVINO processed image but face not found with reason',
                          value=BadResponseDict(additional_info=additional_info_example_no_detect,
                                                result=RESPONSE_CODES['BAD'], reason=Status.codes[Status.NO_FACE])),
    '200_incorrect_image': BadExample(summary='Image is not RGB',
                                      description='OpenVINO can\'t process image because it\'s not RGB or BGR',
                                      value=BadResponseDict(additional_info=additional_info_example_channels_error,
                                                            result=RESPONSE_CODES['BAD'],
                                                            reason=Status.codes[Status.CHANNELS_ERROR])),
    '200_small_face': BadExample(summary='Small face',
                                 description='OpenVINO processed image but face too small',
                                 value=BadResponseDict(additional_info=additional_info_small_face,
                                                       result=RESPONSE_CODES['BAD'],
                                                       reason=Status.codes[Status.SMALL_FACE])),
    '200_mult_faces': BadExample(summary='Multiple faces',
                                 description='OpenVINO detected multiple faces in image',
                                 value=BadResponseDict(additional_info=additional_info_multiple_faces,
                                                       result=RESPONSE_CODES['BAD'],
                                                       reason=Status.codes[Status.MULTIPLE_FACES])),
    '200_bad_angle': BadExample(summary='Bad head angle',
                                description='Head must be in front of camera',
                                value=BadResponseDict(additional_info=additional_info_example_wrong_angle,
                                                      result=RESPONSE_CODES['BAD'],
                                                      reason=Status.codes[Status.CAMERA_TOO_LOW]))

}
responses_400 = {
    '400': BadExample(summary='Incorrect file',
                      description='Uploaded file can\'t be opened',
                      value=BadResponseDict(additional_info=additional_info_example_bad_image,
                                            result=RESPONSE_CODES['BAD'],
                                            reason=Status.codes[Status.IMAGE_ERROR]))
}
post_responses = {
    200:
        {
            'description': 'Image successfully processed by OpenVINO model',
            'content':
                {
                    'application/json':
                        {
                            'examples': responses_200
                        }
                }
        },
    400:
        {
            'description': 'Incorrect image received',
            'content':
                {
                    'application/json':
                        {
                            'example': responses_400
                        }
                }
        }
}
