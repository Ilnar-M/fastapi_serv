FROM python:3.7.9-buster
WORKDIR /fast_api_app
ENV FAST_API_APP_DIR=/fast_api_app
COPY ./requirements.txt /fast_api_app/requirements.txt
RUN apt update && apt install libgl1-mesa-glx libpython3.7 -y
RUN  pip install --no-cache-dir --upgrade -r $FAST_API_APP_DIR/requirements.txt
COPY ./app $FAST_API_APP_DIR
